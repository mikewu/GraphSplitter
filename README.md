# Description

- This program separates supported/unsupported layers into different groups, according to graph topology.

# Run

```bash
$ python3 GraphSplitter.py 
```

