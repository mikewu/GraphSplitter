from collections import deque, defaultdict

'''
tree = {
    1: [12, 2, 3],
    2: [4],
    3: [4],
    4: [13, 5, 11],
    5: [9, 6],
    6: [7],
    7: [8],
    8: [10],
    9: [12],
    10: [12],
    11: [12],
    12: [13],
    13: [14, 16],
    14: [15],
    15: [17],
    16: [17],
    17: [18, 19],
    18: [19],
    19: []
}

unsupported_nodes = [11, 12, 13]
'''

tree = {
    1: [2, 3, 4, 5],
    2: [6],
    3: [7],
    4: [9],
    5: [10],
    6: [15],
    7: [11],
    8: [4],
    9: [12],
    10: [13, 15],
    11: [8, 15],
    12: [15],
    13: [14],
    14: [15],
    15: []
}

unsupported_nodes = [4, 10, 11]

'''
tree = {
    1: [2, 3, 4, 5],
    2: [6],
    3: [7],
    4: [9],
    5: [10],
    6: [16],
    7: [11],
    8: [4],
    9: [12],
    10: [13, 15],
    11: [8, 15],
    12: [15],
    13: [14],
    14: [15],
    15: [],
    16: [17],
    17: [18],
    18: [19],
    19: [20],
    20: [21],
    21: [22],
    22: [23],
    23: [24],
    24: [15]
}

unsupported_nodes = [4, 10, 11]
'''

class GraphSplitter(object):
    def __init__(self, debug=False):
        self.Q = deque()
        self.node2group = dict()
        self.group2nodes = defaultdict(list)
        self.debug = debug

    def run(self):
        root_id = 1
        root_parent_id = -1
        self.Q.append((root_id, root_parent_id))
        while len(self.Q) > 0:
            n, p = self.Q.popleft()

            if self.debug: print('Node {}'.format(n))

            supported_p = True
            supported_n = True

            if n in unsupported_nodes:
                supported_n = False

            if p is None:
                supported_p = supported_n
            else:
                if p in unsupported_nodes:
                    supported_p = False

            if supported_p == supported_n:
                p_group = self.get_group(p)
                n_group = self.get_group(n)
                if n_group >= p_group:
                    # skip traversal
                    continue

                # n_group < p_group
                self.assign_group(n, p_group)
                self.append_childs(n)
                continue

            p_group = self.get_group(p)
            n_group = self.get_group(n)

            if n_group >= p_group + 1:
                # skip traversal
                continue

            # n_group < p_group + 1
            self.assign_group(n, p_group + 1)
            self.append_childs(n)

    def dump_group(self):
        print('=' * 10 + ' BY GROUP ' + '=' * 10)
        for g, nodes in self.group2nodes.items():
            print('[G{}] {}'.format(g, sorted(nodes)))
        print()

    def dump_node(self):
        print('=' * 10 + ' BY NODE ' + '=' * 10)
        for n in sorted(self.node2group.keys()):
            print('N{}: {}'.format(n, self.node2group[n]))
        print()

    def append_childs(self, node_id):
        for c in tree[node_id]:
            self.Q.append((c, node_id))

    def get_group(self, node_id):
        if node_id is -1: # root's parent
            return 1
        if node_id in self.node2group:
            return self.node2group[node_id]
        return -1

    def assign_group(self, node_id, group_id):
        if node_id in self.node2group:
            g0 = self.node2group[node_id]
            if g0 in self.group2nodes:
                self.group2nodes[g0].remove(node_id)
        if self.debug: print('assign N{} to G{}'.format(node_id, group_id))
        self.node2group[node_id] = group_id
        self.group2nodes[group_id].append(node_id)

if __name__ == '__main__':
    splitter = GraphSplitter()
    splitter.run()
    splitter.dump_group()
    splitter.dump_node()